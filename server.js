const express = require('express');
const jsonGraphqlServer = require('json-graphql-server').default;
const {guestList} = require('./example_data.json');

const PORT = 8708;
const app = express();

const guests = guestList.map((guest, index) => {
  // annotate the guest with a couple of extra fields
  const id = index + 1;
  return {
    id,
    emailMarketingOptIn: false,
    ...guest,
  };
});

const data = {guests};

// Small delay to give slightly more real world impression
app.use(function (req, res, next) {
  setTimeout(next, 1000);
});
app.use('/graphql', jsonGraphqlServer(data));
app.listen(PORT);
