export default {
  primary: '#1A535C',
  secondary: '#4ECDC4',
  text: '#F7FFF7',
  tintPrimary: '#FF6B6B',
  tintSecondary: '#FFE66D',
};
