import {Platform} from 'react-native';

export default {
  appName: 'com.IntegrateTestApp',
  // For Android AVD
  host: Platform.OS === 'android' ? '10.0.2.2' : 'localhost',
};
