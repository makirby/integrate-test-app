import React from 'react';
import {ApolloProvider} from '@apollo/client';
import client from '../api/client';
import hoistNonReactStatics from 'hoist-non-react-statics';

export default function ScreenProvider(Screen: React.ComponentType<any>) {
  function ScreenWrapper(props) {
    return (
      <ApolloProvider client={client}>
        <Screen {...props} />
      </ApolloProvider>
    );
  }

  // Need to hoist so react-native-navigation has access to screen options
  return hoistNonReactStatics(ScreenWrapper, Screen);
}
