import {Navigation} from 'react-native-navigation';
import brand from './constants/branding';
import ScreenProvider from './utils/ScreenProvider';
import * as Screens from './screens';
import {StatusBar} from 'react-native';

Navigation.registerComponent('Home', () => ScreenProvider(Screens.Home));
Navigation.registerComponent('AttendeeList', () =>
  ScreenProvider(Screens.AttendeeList),
);
Navigation.registerComponent('AttendeeDetails', () =>
  ScreenProvider(Screens.AttendeeDetails),
);
Navigation.registerComponent('AttendeePreferences', () =>
  ScreenProvider(Screens.AttendeePreferences),
);
Navigation.registerComponent('AttendeeConfirmation', () =>
  ScreenProvider(Screens.AttendeeConfirmation),
);

Navigation.setDefaultOptions({
  topBar: {
    title: {
      color: brand.text,
    },
    backButton: {
      color: brand.text,
    },
    background: {
      color: brand.primary,
    },
  },
  layout: {
    backgroundColor: 'whitesmoke',
  },
});

StatusBar.setBarStyle('light-content');

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        id: 'MAIN',
        children: [
          {
            component: {
              name: 'Home',
            },
          },
        ],
      },
    },
  });
});
