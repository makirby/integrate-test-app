import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {
  Options,
  Navigation,
  NavigationComponentProps,
} from 'react-native-navigation';
import Button from '../components/Button';
import {Guest} from '../api/types';

type Props = {
  guest: Guest;
} & NavigationComponentProps;

export default function AttendeeConfirmation({componentId, guest}: Props) {
  function onContinuePress() {
    Navigation.popToRoot(componentId);
  }

  return (
    <View style={styles.root}>
      <Text style={styles.thankyouText}>Thanks for signing in</Text>
      {guest.emailMarketingOptIn && (
        <Text style={styles.optInText}>
          You've also been opted in for marketing emails
        </Text>
      )}
      <Button
        style={styles.continueButton}
        title="Continue"
        onPress={onContinuePress}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  thankyouText: {
    fontSize: 28,
    fontWeight: '600',
    textAlign: 'center',
  },
  optInText: {
    fontSize: 20,
    color: 'grey',
    fontWeight: '600',
    textAlign: 'center',
  },
  continueButton: {
    marginVertical: 12,
  },
});

AttendeeConfirmation.options = {
  topBar: {
    backButton: {
      visible: false,
    },
    title: {
      text: 'Finished',
    },
  },
  // Block slide to pop so user has to reset stack
  popGesture: false,
} as Options;
