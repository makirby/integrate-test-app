import React, {useState, useMemo} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {
  Options,
  NavigationComponentProps,
  Navigation,
} from 'react-native-navigation';
import {Guest} from '../api/types';
import DataRow from '../components/DataRow';
import DataEntryRow from '../components/DataEntryRow';
import Button from '../components/Button';
import {useMutation} from '@apollo/client';
import {updateEmail} from '../api/mutations';

type Props = {
  guest: Guest;
} & NavigationComponentProps;

export default function AttendeeDetails({componentId, guest}: Props) {
  const [execute, {loading}] = useMutation(updateEmail);
  const [guestDetails, setGuestDetails] = useState({...guest});
  const buttonDisabled = useMemo(() => {
    // Check for email entry, only enable button if it contains at least a character
    // Could be extended to a regex to check for emails in the future
    if (guestDetails.email && guestDetails.email.length >= 1) {
      return false;
    }
    return true;
  }, [guestDetails.email]);

  function onEmailChange(data: string) {
    setGuestDetails({...guestDetails, email: data});
  }

  async function onDetailsConfirmed() {
    // Save the entered guest details incase they have made any changes in the email form
    try {
      const result = await execute({
        variables: {id: guestDetails.id, email: guestDetails.email},
      });

      if (result && !result.errors) {
        Navigation.push(componentId, {
          component: {
            name: 'AttendeePreferences',
            passProps: {
              // Pass response through
              guest: result.data.updateGuest,
            },
          },
        });
      }
    } catch (error) {
      // Error tracing etc to be added
      // Could also use the error object returned by useMutation if we wanted to do something declaratively
      console.error('Something went wrong!', error);
    }
  }

  return (
    <View style={styles.root}>
      <ScrollView keyboardShouldPersistTaps="always">
        <DataRow title="Name" data={guest.name} />
        <DataEntryRow
          title="Email"
          data={guest.email}
          onDataChange={onEmailChange}
        />
        <Button
          disabled={buttonDisabled}
          loading={loading}
          style={styles.buttonStyle}
          onPress={onDetailsConfirmed}
          title="Confirm Details"
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  buttonStyle: {
    margin: 12,
  },
});

AttendeeDetails.options = {
  topBar: {
    title: {
      text: 'Details',
    },
  },
} as Options;
