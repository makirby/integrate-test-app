import React, {useState} from 'react';
import {View, StyleSheet, Text, ScrollView} from 'react-native';
import {
  Options,
  NavigationComponentProps,
  Navigation,
} from 'react-native-navigation';
import {Guest} from '../api/types';
import PreferencesRow from '../components/PreferenceRow';
import Button from '../components/Button';
import {useMutation} from '@apollo/client';
import {updateEmailMarketingPreferences} from '../api/mutations';

type Props = {
  guest: Guest;
} & NavigationComponentProps;

const welcomeText = 'Please select your email preferences below';

export default function AttendeePreferences({componentId, guest}: Props) {
  const [execute, {loading}] = useMutation(updateEmailMarketingPreferences);
  // Prefill the marketing email preferences for the user if they have registered already
  const [emailPreferenceSelected, setEmailPreferenceSelected] = useState(
    guest.emailMarketingOptIn,
  );

  function onPreferenceValueChange(value: boolean) {
    setEmailPreferenceSelected(value);
  }

  async function onConfirmEmailPreferences() {
    // update email preferences for the attendee. Server will now know if the user would like emails
    // sent to them
    try {
      const result = await execute({
        variables: {id: guest.id, enabled: emailPreferenceSelected},
      });

      if (result && !result.errors) {
        Navigation.push(componentId, {
          component: {
            name: 'AttendeeConfirmation',
            passProps: {
              guest: result.data.updateGuest,
            },
          },
        });
      }
    } catch (error) {
      console.error('Failed to update preferences', error);
    }
  }

  return (
    <View style={styles.root}>
      <ScrollView>
        <View style={styles.preferencesWelcome}>
          <Text style={styles.preferencesWelcomeText}>{welcomeText}</Text>
        </View>
        <PreferencesRow
          title="Receive Marketing emails"
          checked={emailPreferenceSelected}
          onPreferenceValueChange={onPreferenceValueChange}
        />
        <Button
          style={styles.buttonStyle}
          title="Confirm Preferences"
          loading={loading}
          onPress={onConfirmEmailPreferences}
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  preferencesWelcome: {
    padding: 12,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'lightgrey',
  },
  preferencesWelcomeText: {
    fontSize: 18,
  },
  buttonStyle: {
    margin: 12,
  },
});

AttendeePreferences.options = {
  topBar: {
    title: {
      text: 'Preferences',
    },
  },
} as Options;
