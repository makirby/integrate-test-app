import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {Options, Navigation} from 'react-native-navigation';

export default function Home({componentId}) {
  function pushAttendeeApp() {
    Navigation.push(componentId, {
      component: {
        name: 'AttendeeList',
      },
    });
  }

  return (
    <View style={styles.root}>
      <Text>Home</Text>
      <View style={styles.welcome}>
        <Text style={styles.centerText}>Welcome to my Integrate Tech Test</Text>
        <Text style={styles.centerText}>
          This is just a landing screen, it would be another part of the
          application that forwards you to the attendee list. You can access the
          list using the button below
        </Text>
      </View>

      <Button title="Start Attendee App" onPress={pushAttendeeApp} />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome: {
    margin: 12,
  },
  centerText: {
    textAlign: 'center',
  },
});

Home.options = {
  topBar: {
    title: {
      text: 'Home',
    },
  },
} as Options;
