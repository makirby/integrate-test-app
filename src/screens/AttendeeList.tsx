import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {Options, Navigation} from 'react-native-navigation';
import {useQuery} from '@apollo/client';
import {listGuests} from '../api/queries';
import Loader from '../components/Loader';
import AttendeeListRow from '../components/AttendeeListRow';
import {Guest} from '../api/types';

export default function AttendeeList({componentId}) {
  const {data, error, loading} = useQuery(listGuests);

  function onSelectGuest(guest: Guest) {
    Navigation.push(componentId, {
      component: {
        name: 'AttendeeDetails',
        passProps: {
          guest,
        },
      },
    });
  }

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <View style={styles.root}>
        <Text>Error occured: is server.js running?</Text>
      </View>
    );
  }

  return (
    <View style={styles.root}>
      <ScrollView>
        {data.allGuests.map((guest, index) => (
          <AttendeeListRow
            key={index}
            guest={guest}
            index={index}
            onRowPress={onSelectGuest}
          />
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});

AttendeeList.options = {
  topBar: {
    title: {
      text: 'Attendees',
    },
  },
} as Options;
