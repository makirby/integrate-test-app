import gql from 'graphql-tag';

const listGuests = gql`
  query ListGuests {
    allGuests {
      id
      name
      email
      emailMarketingOptIn
    }
  }
`;

export {listGuests};
