import {ApolloClient, InMemoryCache} from '@apollo/client';
import config from '../constants/config';

export default new ApolloClient({
  uri: `http://${config.host}:8708/graphql`,
  cache: new InMemoryCache(),
  name: config.appName,
});
