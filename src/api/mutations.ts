import gql from 'graphql-tag';

const updateEmail = gql`
  mutation updateEmail($id: ID!, $email: String) {
    updateGuest(id: $id, email: $email) {
      id
      name
      email
      emailMarketingOptIn
    }
  }
`;

const updateEmailMarketingPreferences = gql`
  mutation updateEmailMarketingPreferences($id: ID!, $enabled: Boolean!) {
    updateGuest(id: $id, emailMarketingOptIn: $enabled) {
      id
      name
      email
      emailMarketingOptIn
    }
  }
`;

export {updateEmail, updateEmailMarketingPreferences};
