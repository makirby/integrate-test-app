import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

type Props = {
  title: string;
  data: string;
};

export default function DataRow({title, data}: Props) {
  return (
    <View style={styles.root}>
      <View style={styles.titleRow}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <View style={styles.dataRow}>
        <Text style={styles.dataText}>{data}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    padding: 12,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'lightgrey',
  },
  titleRow: {
    flex: 1,
    marginBottom: 8,
  },
  titleText: {
    fontSize: 28,
  },
  dataRow: {
    flex: 1,
  },
  dataText: {
    fontSize: 20,
    color: 'grey',
  },
});
