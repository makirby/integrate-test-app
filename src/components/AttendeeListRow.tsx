import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import {Guest} from '../api/types';

type Props = {
  guest: Guest;
  index: number;
  onRowPress: (guest: Guest) => void;
};

export default function AttendeeListRow({guest, index, onRowPress}: Props) {
  function onPress() {
    onRowPress(guest);
  }

  return (
    <View style={[styles.root, index !== 0 && styles.border]}>
      <TouchableHighlight
        underlayColor="whitesmoke"
        style={styles.textWrapper}
        onPress={onPress}>
        <Text style={styles.text}>{guest.name}</Text>
      </TouchableHighlight>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    height: 55,
  },
  border: {
    borderTopColor: 'lightgrey',
    borderTopWidth: StyleSheet.hairlineWidth,
  },
  textWrapper: {
    flexDirection: 'row',
    flex: 1,
    padding: 12,
  },
  text: {
    flex: 1,
    fontSize: 22,
  },
});
