import React from 'react';
import {StyleSheet, ActivityIndicator, View} from 'react-native';

export default function Loader() {
  return (
    <View style={styles.root}>
      <ActivityIndicator />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
