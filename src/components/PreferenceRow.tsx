import React from 'react';
import {StyleSheet, View, Text, Switch} from 'react-native';
import branding from '../constants/branding';

type Props = {
  title: string;
  checked: boolean;
  onPreferenceValueChange: (value: boolean) => void;
};

export default function PreferenceRow({
  title,
  checked,
  onPreferenceValueChange,
}: Props) {
  return (
    <View style={styles.root}>
      <View style={styles.titleRow}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <View style={styles.preferenceRow}>
        <Switch
          value={checked}
          onValueChange={onPreferenceValueChange}
          trackColor={{true: branding.secondary, false: 'lightgrey'}}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    padding: 12,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'lightgrey',
  },
  titleRow: {
    flex: 1,
    marginBottom: 8,
  },
  titleText: {
    fontSize: 28,
  },
  preferenceRow: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
});
