import React from 'react';
import Button from '../Button';
import TestRenderer from 'react-test-renderer';
import {ActivityIndicator, Text} from 'react-native';

describe('Button', () => {
  it('displays the specified title', () => {
    const mockPress = jest.fn();
    const title = 'This is a button title';
    const button = TestRenderer.create(
      <Button title={title} onPress={mockPress} />,
    );
    const buttonInstance = button.root;

    expect(buttonInstance.findByType(Text).props.children).toEqual(title);
  });

  it('displays an ActivityIndicator when loading', () => {
    const mockPress = jest.fn();
    const button = TestRenderer.create(<Button onPress={mockPress} loading />);
    const buttonInstance = button.root;

    expect(buttonInstance.findByType(ActivityIndicator)).not.toBeFalsy();
  });
});
