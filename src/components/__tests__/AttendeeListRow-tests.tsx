import React from 'react';
import AttendeeListRow from '../AttendeeListRow';
import TestRenderer from 'react-test-renderer';

describe('AttendeeListRow', () => {
  const mockPress = jest.fn();
  const mockGuest = {
    id: 1,
    name: 'Test Name',
    email: 'Test@example.com',
    emailMarketingOptIn: false,
  };
  it('will render correctly in a snapshot', () => {
    const attendeeListRow = TestRenderer.create(
      <AttendeeListRow index={0} guest={mockGuest} onRowPress={mockPress} />,
    );

    expect(attendeeListRow.toJSON()).toMatchSnapshot();
  });

  it('will render with a border when not the first element in a list', () => {
    const attendeeListRow = TestRenderer.create(
      <AttendeeListRow index={1} guest={mockGuest} onRowPress={mockPress} />,
    );

    expect(attendeeListRow.toJSON()).toMatchSnapshot();
  });
});
