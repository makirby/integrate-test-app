import React from 'react';
import {
  Pressable,
  View,
  Text,
  StyleSheet,
  Platform,
  PressableProps,
  ActivityIndicator,
} from 'react-native';
import brand from '../constants/branding';

export type Props = {
  title?: string;
  disabled?: boolean;
  loading?: boolean;
  onPress: () => void;
} & Omit<PressableProps, 'children'>;

export default function Button({title, disabled, loading, ...other}: Props) {
  return (
    <Pressable disabled={disabled || loading} {...other}>
      {({pressed}) => (
        <View
          style={[
            styles.button,
            pressed && !loading && styles.buttonPressed,
            disabled && styles.buttonDisabled,
          ]}>
          {loading ? (
            <ActivityIndicator color={brand.text} />
          ) : (
            <Text style={styles.text}>{title}</Text>
          )}
        </View>
      )}
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    height: Platform.OS === 'ios' ? 44 : 55,
    minWidth: 140,
    backgroundColor: brand.secondary,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonDisabled: {
    opacity: 0.4,
  },
  buttonPressed: {
    opacity: 0.77,
  },
  text: {
    fontSize: 19,
    color: brand.text,
  },
});
