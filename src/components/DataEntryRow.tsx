import React from 'react';
import {StyleSheet, View, Text, TextInput} from 'react-native';

type Props = {
  title: string;
  data?: string;
  onDataChange: (data: string) => void;
};

export default function DataEntryRow({title, data, onDataChange}: Props) {
  return (
    <View style={styles.root}>
      <View style={styles.titleRow}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <View style={styles.dataRow}>
        <TextInput
          placeholder={`Please enter an ${title.toLowerCase()}`}
          clearButtonMode="while-editing"
          style={styles.dataText}
          onChangeText={onDataChange}>
          {data}
        </TextInput>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    padding: 12,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'lightgrey',
  },
  titleRow: {
    flex: 1,
    marginBottom: 8,
  },
  titleText: {
    fontSize: 28,
  },
  dataRow: {
    flex: 1,
  },
  dataText: {
    fontSize: 20,
  },
});
