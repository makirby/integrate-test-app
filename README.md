# IntegrateTestApp

Hi there, hope my attempt is sufficient. I decided to use some tools that i'd heard about but hadn't
used yet and it seems to have worked out ok! 

## Overview

To run: `yarn start` will run the mock server and react-native bundler


### Tools/packages
- Typescript
- GraphQl: can create simple server out of the json provided
- apollo: easiest way (i've found sofar) to consume GraphQL servers 

### Architecture
Basic structure overview
```
src/
  api/
    - Any files related to the GraphQL server including apollo client
    - 3rd party clients would be added here as well
  screens/
    - Stores the top level screens in the application. 
    - These are registered to react-naive-navigation 
      to allow navigation to/from
    - Composed using a set of components
    - Contains graphQl requests and loading/error handling etc
  components/
    - Smaller self contained parts of the application
    - Reusable within screens
  constants/
    - Contains any configuration etc
    - in a real world app this would be changable based on environment
```

### Mock API
Using the `json-graphql-server` package we could quickly mock the required server and any responses. The data will persist in the server on mutations so edits to user data will persist untill restarted.

As its based on express I was able to add a short delay quite easily to mimic a real world request slightly better.

The email API is handled by the request setting the users preferences. A seperate email dispatch would handle sending the emails out to the user

### Testing
- Made use of some simples snapshots of the components
- Example of simple testting using react-test-renderer
- Ideally would use some E2E tooling (detox) to test the screens (if there was time)